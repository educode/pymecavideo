<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en" sourcelanguage="fr">
<context>
    <name>@default</name>
    <message>
        <location filename="pymecavideo.py" line="277"/>
        <source>Lancer %1
 pour capturer une vidÃ©o</source>
        <translation type="obsolete">Launch %1
to capture a video</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="preferences.ui" line="13"/>
        <source>Préférences de pyMecaVideo</source>
        <translation type="obsolete">Preferences for Pymecavideo</translation>
    </message>
    <message>
        <location filename="preferences.ui" line="19"/>
        <source>Échelle des vitesses (px pour 1m/s)</source>
        <translation type="obsolete">Scale for velocities (px by m/s)</translation>
    </message>
    <message>
        <location filename="preferences.ui" line="29"/>
        <source>Vitesses affichées</source>
        <translation type="obsolete">Display velocities</translation>
    </message>
    <message>
        <location filename="preferences.ui" line="39"/>
        <source>Afficheur vidéo</source>
        <translation type="obsolete">Video player</translation>
    </message>
    <message>
        <location filename="preferences.ui" line="49"/>
        <source>Niveau de verbosité (débogage)</source>
        <translation type="obsolete">Verbosity level (debugging)</translation>
    </message>
</context>
<context>
    <name>Label_Echelle</name>
    <message>
        <location filename="echelle.py" line="109"/>
        <source>Choisir le nombre de points puis &amp;quot;DÃ©marrer l'acquisition&amp;quot; </source>
        <translation type="obsolete">Choose the number of points then \&amp;quot;Start aquisition\&amp;quot;</translation>
    </message>
</context>
<context>
    <name>MontreFilm</name>
    <message>
        <location filename="../../src/cadreur.py" line="211"/>
        <source>Voir la vidéo</source>
        <translation>Watch the video</translation>
    </message>
    <message>
        <location filename="../../src/cadreur.py" line="212"/>
        <source>Ralenti : 1/1</source>
        <translation>Slow motion: 1/1</translation>
    </message>
</context>
<context>
    <name>StartQT4</name>
    <message>
        <location filename="pymecavideo.py" line="379"/>
        <source>indÃ©f.</source>
        <translation type="obsolete">undef.</translation>
    </message>
    <message>
        <location filename="__init__.py" line="336"/>
        <source>chemin vers les modules : %s</source>
        <translation type="obsolete">path to the modules: %s</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1041"/>
        <source>temps en seconde, positions en mÃ¨tre</source>
        <translation type="obsolete">time in second, positions in meter</translation>
    </message>
    <message>
        <location filename="__init__.py" line="448"/>
        <source>point NÂ° </source>
        <translation type="obsolete">Point N° </translation>
    </message>
    <message>
        <location filename="__init__.py" line="722"/>
        <source>Cliquer sur le point NÂ°%d</source>
        <translation type="obsolete">Clic on the point #%d</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1518"/>
        <source>DÃ©finir une Ã©chelle</source>
        <translation type="obsolete">Define a scale</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1518"/>
        <source>Quelle est la longueur en mÃ¨tre de votre Ã©talon sur l'image ?</source>
        <translation type="obsolete">Which the length (in meter) of your gauge in the image?</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1536"/>
        <source> Merci d'indiquer une Ã©chelle valable</source>
        <translation type="obsolete">Please give a valid scale</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1592"/>
        <source>Les donnÃ©es seront perdues</source>
        <translation type="obsolete">Data will be lost</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1592"/>
        <source>Votre travail n'a pas Ã©tÃ© sauvegardÃ©
Voulez-vous les sauvegarder ?</source>
        <translation type="obsolete">Your work has not been saved. Do you want to save it?</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1614"/>
        <source>Vous avez atteint le dÃ©but de la vidÃ©o</source>
        <translation type="obsolete">You reached the begin of the video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1650"/>
        <source>Ouvrir une vidÃ©o</source>
        <translation type="obsolete">Open a video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1645"/>
        <source>Nom de fichier non conforme</source>
        <translation type="obsolete">Unvalid filename</translation>
    </message>
    <message>
        <location filename="__init__.py" line="895"/>
        <source>Le nom de votre fichier contient des caractÃ¨res accentuÃ©s ou des espaces.
 Merci de bien vouloir le renommer avant de continuer</source>
        <translation type="obsolete">Your filename contains accented characters or spaces. Please rename it before going further</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1677"/>
        <source>Veuillez choisir une image et dÃ©finir l'Ã©chelle</source>
        <translation type="obsolete">Please select an image and define the scale</translation>
    </message>
    <message>
        <location filename="__init__.py" line="938"/>
        <source>DÃ©solÃ© pas de fichier d'aide pour ce langage %s.</source>
        <translation type="obsolete">Sorry, no help file for this language %s </translation>
    </message>
    <message>
        <location filename="__init__.py" line="1010"/>
        <source>Impossible de lire %s</source>
        <translation type="obsolete">Read %s failed</translation>
    </message>
    <message>
        <location filename="__init__.py" line="1029"/>
        <source>Usage : pymecavideo [-f fichier | --fichier_pymecavideo=fichier]</source>
        <translation type="obsolete">Usage : pymecavideo [-f file | --fichier_pymecavideo=file]</translation>
    </message>
    <message>
        <location filename="__init__.py" line="438"/>
        <source>point NÂ°</source>
        <translation type="obsolete">point #</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="277"/>
        <source>Lancer %1
 pour capturer une vidÃ©o</source>
        <translation type="obsolete">Launch %1
to capture a video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="308"/>
        <source>indÃ©f</source>
        <translation type="obsolete">undef</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="352"/>
        <source>NON DISPO : %1</source>
        <translation type="obsolete">NOT AVAIL.: %1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="869"/>
        <source>Ouvrir un projet Pymecavideo</source>
        <translation type="obsolete">Open a Mecavideo project</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="869"/>
        <source>fichiers pymecavideo(*.csv)</source>
        <translation type="obsolete">pymecavideo files (*.csv)</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1104"/>
        <source>point NÂ° %1</source>
        <translation type="obsolete">point # %1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1115"/>
        <source>Veuillez sÃ©lectionner un cadre autour de(s) l'objet(s) que vous voulez suivre.
Vous pouvez arrÃªter Ã  tous moments la capture en appuyant sur le bouton</source>
        <translation type="obsolete">PLease select a frame around the object(s) which you will track.
You can stop the capture at any moment by clicking the button</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1262"/>
        <source>Choisir ...</source>
        <translation type="obsolete">Choose...</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1283"/>
        <source>Evolution de l&apos;abscisse du point %1</source>
        <translation type="obsolete">Evolution of the abscissa of point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1283"/>
        <source>Evolution de l'ordonnÃ©e du point %1</source>
        <translation type="obsolete">Evolution of ordinate of point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1283"/>
        <source>Evolution de la vitesse du point %1</source>
        <translation type="obsolete">Evolution of speed of point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1345"/>
        <source>Pointage des positionsÂ : cliquer sur le point NÂ° %1</source>
        <translation type="obsolete">Sampling positions: click on point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1376"/>
        <source>Vous avez atteint la fin de la vidÃ©o</source>
        <translation type="obsolete">You reached the end of the video</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1627"/>
        <source>fichiers vidÃ©os ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</source>
        <translation type="obsolete">video files (*.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1636"/>
        <source>fichiers vidÃ©os ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</source>
        <translation type="obsolete">video files( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov) </translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1645"/>
        <source>Le nom de votre fichier contient des caractÃ¨res accentuÃ©s ou des espaces.
Merci de bien vouloir le renommer avant de continuer</source>
        <translation type="obsolete">The name of the file contains accented characters or spaces.
PLease rename it before going on</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1711"/>
        <source>DÃ©solÃ© pas de fichier d'aide pour le langage %1.</source>
        <translation type="obsolete">Sorry, no help file for the language %1.</translation>
    </message>
</context>
<context>
    <name>pymecavideo</name>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="37"/>
        <source>PyMecaVideo, analyse mécanique des vidéos</source>
        <translation>PyMecaVideo; analysing mechanics in videos</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="86"/>
        <source>Acquisition des données</source>
        <translation type="obsolete">Data acquisition</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="189"/>
        <source>Pas de vidéos chargées</source>
        <translation type="obsolete">No video loaded</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="144"/>
        <source>Bienvenue sur pymeca vidéo, pas d'images chargée</source>
        <translation type="obsolete">Welcome in pymecavideo, no video loaded</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="533"/>
        <source>Définir l'échelle</source>
        <translation>Define the scale</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="926"/>
        <source>Image n°</source>
        <translation>Image #</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="246"/>
        <source>Nombre de points à étudier</source>
        <translation type="obsolete">Number of points to study</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="480"/>
        <source>indéf.</source>
        <translation>undef.</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="236"/>
        <source>px/m</source>
        <translation>px/m</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="297"/>
        <source>Démarrer l'acquisition</source>
        <translation type="obsolete">Start acquisition</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="384"/>
        <source>Tout réinitialiser</source>
        <translation>Reinitialize</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="390"/>
        <source>efface le point précédent</source>
        <translation type="obsolete">delete previous points</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="455"/>
        <source>rétablit le point suivant</source>
        <translation type="obsolete">restore next points</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="426"/>
        <source>trajectoires et mesures</source>
        <translation type="obsolete">trajectory and measurements</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="526"/>
        <source>Origine du référentiel :</source>
        <translation type="obsolete">Origin of the axis:</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="560"/>
        <source>Vidéo calculée</source>
        <translation type="obsolete">Computed video</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="568"/>
        <source>V. normale</source>
        <translation type="obsolete">Normal Vel</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="573"/>
        <source>ralenti /2</source>
        <translation type="obsolete">slower /2</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="578"/>
        <source>ralenti /4</source>
        <translation type="obsolete">slower /4</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="583"/>
        <source>ralenti /8</source>
        <translation type="obsolete">slower /8</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="608"/>
        <source>Échelle de vitesses :</source>
        <translation type="obsolete">Scale for velocities:</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="882"/>
        <source>px pour 1 m/s</source>
        <translation>px for 1 m/s</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1089"/>
        <source>Coordonnées</source>
        <translation>Coordinates</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1125"/>
        <source>Copier les mesures dans le presse papier</source>
        <translation>Copy data to the clipboard</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="682"/>
        <source>Fichier</source>
        <translation type="obsolete">File</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="753"/>
        <source>Aide</source>
        <translation type="obsolete">Help</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="703"/>
        <source>Édition</source>
        <translation type="obsolete">Edit</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2767"/>
        <source>Ouvrir une vidéo</source>
        <translation>Open a video</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1620"/>
        <source>avanceimage</source>
        <translation>imgforward</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1625"/>
        <source>reculeimage</source>
        <translation>imgbackward</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="738"/>
        <source>Quitter</source>
        <translation type="obsolete">Quit</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="743"/>
        <source>Enregistrer les données</source>
        <translation type="obsolete">Save data</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="748"/>
        <source>À propos</source>
        <translation type="obsolete">About</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="1149"/>
        <source>Exemples ...</source>
        <translation type="obsolete">Examples ...</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="763"/>
        <source>Rouvrir un fichier mecavidéo</source>
        <translation type="obsolete">Reopen a mecavideo file</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="768"/>
        <source>Préférences</source>
        <translation type="obsolete">Preferences</translation>
    </message>
    <message>
        <location filename="pymecavideo.ui" line="773"/>
        <source>copier dans le presse-papier</source>
        <translation type="obsolete">copy to the clipboard</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="280"/>
        <source>Acquisition video</source>
        <translation type="obsolete">Acquire vIdeo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="123"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="209"/>
        <source>Acquisition</source>
        <translation type="obsolete">Acquisition</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="287"/>
        <source>Démarrer</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="418"/>
        <source>efface la série précédente</source>
        <translation type="obsolete">delete previous series</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="621"/>
        <source>Points à 
 étudier:</source>
        <translation type="obsolete">Points to 
study:</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="639"/>
        <source>suivi
automatique</source>
        <translation type="obsolete">automatic
tracking</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="404"/>
        <source>Changer d&apos;origine</source>
        <translation>Change the origin</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="414"/>
        <source>Abscisses 
vers la gauche</source>
        <translation type="unfinished">Abscissa to the left
</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="787"/>
        <source>Trajectoires</source>
        <translation>Trajectories</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="782"/>
        <source>Montrer 
les vecteurs
vitesses</source>
        <translation type="obsolete">Show 
velocity 
vectors</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="791"/>
        <source>près de
la souris</source>
        <translation type="obsolete">near the
mouse</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="852"/>
        <source>partout</source>
        <translation>everywhere</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="875"/>
        <source>Échelle de vitesses</source>
        <translation>Scale for speed</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="909"/>
        <source>Voir un graphique</source>
        <translation type="obsolete">See a plot</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2013"/>
        <source>Choisir ...</source>
        <translation>Choose...</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1056"/>
        <source>Voir la vidéo</source>
        <translation>Watch the video</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="966"/>
        <source>Définir un autre référentiel : </source>
        <translation type="obsolete">Define another reference frame</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1095"/>
        <source>Tableau des dates et des coordonnées</source>
        <translation>Table of times and coordinates</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1058"/>
        <source>Exporter vers ....</source>
        <translation type="obsolete">Export to ...</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="997"/>
        <source>Oo.o Calc</source>
        <translation type="obsolete">Oo.o Calc</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1073"/>
        <source>Qtiplot</source>
        <translation type="obsolete">Qtiplot</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1078"/>
        <source>SciDAVis</source>
        <translation type="obsolete">SciDAVis</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1015"/>
        <source>changer d'échelle ?</source>
        <translation type="obsolete">change scale?</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1559"/>
        <source>&amp;Fichier</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1563"/>
        <source>E&amp;xporter vers ...</source>
        <translation>E&amp;xport to ...</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1670"/>
        <source>&amp;Aide</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1591"/>
        <source>&amp;Edition</source>
        <translation>&amp;Edit</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1102"/>
        <source>&amp;Ouvrir une vidéo (Ctrl-O)</source>
        <translation type="obsolete">&amp;Open video (Ctrl-O)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1120"/>
        <source>Quitter (Ctrl-Q)</source>
        <translation type="obsolete">Quit (Ctrl-Q)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1128"/>
        <source>Enregistrer les données (Ctrl-S)</source>
        <translation type="obsolete">Save data (Ctrl-S)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1661"/>
        <source>À &amp;propos</source>
        <translation>&amp;About</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1141"/>
        <source>Aide (F1)</source>
        <translation type="obsolete">Help (F1)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1154"/>
        <source>Ouvrir un projet &amp;mecavidéo</source>
        <translation type="obsolete">Open a &amp;mecavideo project</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1696"/>
        <source>&amp;Préférences</source>
        <translation>&amp;Preferences</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1531"/>
        <source>&amp;Copier dans le presse-papier (Ctrl-C)</source>
        <translation type="obsolete">&amp;Copy to clipboard (Ctrl-C)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1172"/>
        <source>Défaire (Ctrl-Z)</source>
        <translation type="obsolete">Undo (Ctrl-Z)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1180"/>
        <source>Refaire (Ctrl-Y)</source>
        <translation type="obsolete">Redo (Ctrl-Y)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1188"/>
        <source>OpenOffice.org &amp;Calc</source>
        <translation type="obsolete">OpenOffice.org &amp;Calc</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1748"/>
        <source>Qti&amp;plot</source>
        <translation>Qti&amp;plot</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1753"/>
        <source>Sci&amp;davis</source>
        <translation>Sci&amp;davis</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="314"/>
        <source>Lancer %1
 pour capturer une vidÃ©o</source>
        <translation type="obsolete">Launch %1
to capture a video</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="387"/>
        <source>indÃ©f</source>
        <translation type="obsolete">undef</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="332"/>
        <source>NON DISPO : %1</source>
        <translation type="obsolete">NOT AVAIL.: %1</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1125"/>
        <source>Ouvrir un projet Pymecavideo</source>
        <translation>Open a Mecavideo project</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1221"/>
        <source>fichiers pymecavideo(*.csv)</source>
        <translation type="obsolete">pymecavideo files (*.csv)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1397"/>
        <source>temps en seconde, positions en mÃ¨tre</source>
        <translation type="obsolete">time in second, positions in meter</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1245"/>
        <source>point NÂ° %1</source>
        <translation type="obsolete">point # %1</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1452"/>
        <source>Veuillez sÃ©lectionner un cadre autour de(s) l'objet(s) que vous voulez suivre.
Vous pouvez arrÃªter Ã  tous moments la capture en appuyant sur le bouton</source>
        <translation type="obsolete">PLease select a frame around the object(s) which you will track.
You can stop the capture at any moment by clicking the button</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1432"/>
        <source>Evolution de l&apos;abscisse du point %1</source>
        <translation type="obsolete">Evolution of the abscissa of point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1432"/>
        <source>Evolution de l'ordonnÃ©e du point %1</source>
        <translation type="obsolete">Evolution of ordinate of point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1432"/>
        <source>Evolution de la vitesse du point %1</source>
        <translation type="obsolete">Evolution of speed of point #%1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1496"/>
        <source>Pointage des positionsÂ : cliquer sur le point NÂ° %1</source>
        <translation type="obsolete">Sampling positions: click on point #%1</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2204"/>
        <source>Vous avez atteint la fin de la vidÃ©o</source>
        <translation type="obsolete">You reached the end of the video</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2551"/>
        <source>DÃ©finir une Ã©chelle</source>
        <translation type="obsolete">Define a scale</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2530"/>
        <source>Quelle est la longueur en mÃ¨tre de votre Ã©talon sur l'image ?</source>
        <translation type="obsolete">Which the length (in meter) of your gauge in the image?</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2552"/>
        <source> Merci d'indiquer une Ã©chelle valable</source>
        <translation type="obsolete"> Please give a valid scale</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2613"/>
        <source>Les donnÃ©es seront perdues</source>
        <translation type="obsolete">Data will be lost</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2613"/>
        <source>Votre travail n'a pas Ã©tÃ© sauvegardÃ©
Voulez-vous les sauvegarder ?</source>
        <translation type="obsolete">Your work has not been saved.
Do you want to save it?</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1782"/>
        <source>Vous avez atteint le dÃ©but de la vidÃ©o</source>
        <translation type="obsolete">You reached the begin of the video</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2762"/>
        <source>Nom de fichier non conforme</source>
        <translation>Unvalid filename</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2702"/>
        <source>Le nom de votre fichier contient des caractÃ¨res accentuÃ©s ou des espaces.
Merci de bien vouloir le renommer avant de continuer</source>
        <translation type="obsolete">The name of the file contains accented characters or spaces.
PLease rename it before going on</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1862"/>
        <source>Veuillez choisir une image et dÃ©finir l'Ã©chelle</source>
        <translation type="obsolete">Please select an image and define the scale</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1907"/>
        <source>DÃ©solÃ© pas de fichier d'aide pour le langage %1.</source>
        <translation type="obsolete">Sorry, no help file for the language %1.</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1798"/>
        <source>fichiers vidÃ©os ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</source>
        <translation type="obsolete">video files (*.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2688"/>
        <source>fichiers vidÃ©os ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</source>
        <translation type="obsolete">video files( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="660"/>
        <source>Enregistrer la chronophotographie</source>
        <translation type="obsolete">Save the chronophotography</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="688"/>
        <source>fichiers images(*.png *.jpg)</source>
        <translation>Image files (*.png *.jpg)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="446"/>
        <source>NON DISPO : {0}</source>
        <translation type="obsolete">UNAVAILABLE: {0}</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1495"/>
        <source>point N° {0}</source>
        <translation>point # {0}</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2016"/>
        <source>Evolution de l&apos;abscisse du point {0}</source>
        <translation type="obsolete">Plot of point #{0} abscissa</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1593"/>
        <source>Evolution de l'ordonnée du point {0}</source>
        <translation type="obsolete">Plot of point #{0} ordinate</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2016"/>
        <source>Evolution de la vitesse du point {0}</source>
        <translation type="obsolete">Plot of point #{0} velocity</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2227"/>
        <source>Pointage des positions : cliquer sur le point N° {0}</source>
        <translation type="obsolete">Input position: please click on point # {0}</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2833"/>
        <source>Désolé pas de fichier d'aide pour le langage {0}.</source>
        <translation>Sorry, no help file for language {0}.</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2731"/>
        <source>fichiers vidéos (*.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</source>
        <translation>video files (*.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2798"/>
        <source>Veuillez choisir une image (et définir l'échelle)</source>
        <translation>Please choose an image (and define the scale)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2913"/>
        <source>Pymecavideo n'arrive pas à lire l'image</source>
        <translation>Pymecavideo cannot read the image</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="882"/>
        <source>Fichier Python créé</source>
        <translation type="obsolete">Python file created</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1519"/>
        <source>Pointage Automatique</source>
        <translation>Auto tracking</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1114"/>
        <source>le format de cette vidéo n'est pas pris en charge par pymecavideo</source>
        <translation>the format of this video is not managed by pymecavideo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1293"/>
        <source>Le fichier choisi n&apos;est pas compatible avec pymecavideo</source>
        <translation>The chosen file is not compatible with pymecavideo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2385"/>
        <source>Masse de l&apos;objet</source>
        <translation>Mass of the object</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2385"/>
        <source>Quelle est la masse de l&apos;objet ? (en kg)</source>
        <translation>Which is the object mass? (in kg)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2403"/>
        <source> Merci d&apos;indiquer une masse valable</source>
        <translation> Please provide a valuable mass</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2709"/>
        <source>MAUVAISE VALEUR !</source>
        <translation>BAD VALUE!</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2709"/>
        <source>La valeur rentrée n'est pas compatible avec le calcul</source>
        <translation>The given value is not compatible with calculations</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="842"/>
        <source>près de la souris</source>
        <translation>near the mouse</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1484"/>
        <source>Enregistrer</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1379"/>
        <source>9.8</source>
        <translation>9.8</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1326"/>
        <source>Grapheur</source>
        <translation>Plotting</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1423"/>
        <source>en fonction de </source>
        <translation>as a function of </translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1682"/>
        <source>&amp;Exemples ...</source>
        <translation>&amp;Examples ...</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1743"/>
        <source>LibreOffice &amp;Calc</source>
        <translation>LibreOffice &amp;Calc</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1758"/>
        <source>&amp;Python (source)</source>
        <translation>&amp;Python (source)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="451"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nombre d&apos;Images Par Seconde&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p&amp;gt;Number of frames per second&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="454"/>
        <source>IPS :</source>
        <translation>FPS:</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="476"/>
        <source>000</source>
        <translation>000</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="604"/>
        <source>Tourner l'image de 90° vers la gauche</source>
        <translation>Turn the image 90° leftward</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="633"/>
        <source>Tourner l'image de 90° vers la droite</source>
        <translation>Turn the image 90° rightward</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="815"/>
        <source>Trajectoire</source>
        <translation>Trajectory</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="912"/>
        <source>Chronophotographie</source>
        <translation>Chronophotography</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="825"/>
        <source>Chronogramme</source>
        <translation>Chronogram</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1359"/>
        <source>1.0</source>
        <translation>1.0</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1260"/>
        <source>g (N/kg)</source>
        <translation>g (N/kg)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1433"/>
        <source>avec le style </source>
        <translation>with the style</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1441"/>
        <source>Points seuls</source>
        <translation>Points only</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1450"/>
        <source>Points et lignes</source>
        <translation>Points and lines</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1459"/>
        <source>Lignes seules</source>
        <translation>Lines only</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1763"/>
        <source>&amp;Fichier numpy</source>
        <translation>&amp;Numpy file</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1766"/>
        <source>Fichier Numpy</source>
        <translation>Numpy File</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="688"/>
        <source>Enregistrer comme image</source>
        <translation>Save as an image</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2176"/>
        <source>Erreur lors de l&apos;enregistrement</source>
        <translation>Error during saving</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2587"/>
        <source>Définir léchelle</source>
        <translation>Define the scale</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2858"/>
        <source>Le nombre d'images par seconde doit être un entier</source>
        <translation>Le number of frames per second must be an integer</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2858"/>
        <source>merci de recommencer</source>
        <translation>please try again</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="297"/>
        <source>STOP</source>
        <translation>STOP</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="707"/>
        <source>Efface le point précédent</source>
        <translation>Erase the previous point</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="745"/>
        <source>Rétablit le point suivant</source>
        <translation>Unerase the next point</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1521"/>
        <source>Veuillez sélectionner un cadre autour du ou des objets que vous voulez suivre.
Vous pouvez arrêter à tout moment la capture en appuyant sur le bouton STOP</source>
        <translation>Please select a frame around the object(s) which you want to follow
You can the automatic capture at any time by clicking on the STOP button</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1612"/>
        <source>&amp;Ouvrir une vidéo</source>
        <translation>&amp;Open a video file</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1634"/>
        <source>&amp;Quitter</source>
        <translation>&amp;Quit</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1705"/>
        <source>&amp;Copier dans le presse-papier</source>
        <translation>&amp;Copy to the clipboard</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1717"/>
        <source>&amp;Défaire</source>
        <translation>&amp;Undo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1735"/>
        <source>Refaire</source>
        <translation>Redo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1729"/>
        <source>&amp;Refaire</source>
        <translation>&amp;Redo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1183"/>
        <source>Changer d'échelle</source>
        <translation>Modify the scale</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1413"/>
        <source>Tracer :</source>
        <translation>Plot:</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1208"/>
        <source>Ajouter les énergies :</source>
        <translation>Add the energies:</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1228"/>
        <source>Cinétique (échelle obligatoire)</source>
        <translation>Cinetic (scale is mandatory)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1235"/>
        <source>Potentielle de pesanteur</source>
        <translation>Potential (gravity)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1242"/>
        <source>Mécanique</source>
        <translation>Mecanical</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="277"/>
        <source>Suivi automatique</source>
        <translation>Automatic capture</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1215"/>
        <source>Intensité de la pesanteur :</source>
        <translation>Gravity:</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1338"/>
        <source>Données et grandeurs à représenter</source>
        <translation>Data and values to plot</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1346"/>
        <source>Masse (kg)</source>
        <translation>Mass (kg)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1366"/>
        <source>Intensité de la pesanteur g (N/kg)</source>
        <translation>Gravity g (N/kg)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="102"/>
        <source>Pointage</source>
        <translation>Capture</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1006"/>
        <source>Changement de référentiel : </source>
        <translation>Change the referential frame:</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1646"/>
        <source>&amp;Enregistrer le projet mecavideo</source>
        <translation>&amp;Save the project in pymecavideo format</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1691"/>
        <source>Ouvrir un projet &amp;mecavideo</source>
        <translation>Open a projet in pymecavideo format</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2176"/>
        <source>Echec de l&apos;enregistrement du fichier:&lt;b&gt;
{0}&lt;/b&gt;</source>
        <translation>Saving the file:&amp;lt;b&amp;gt;
{0}&amp;lt;/b&amp;gt; failed</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2168"/>
        <source>Enregistrer le graphique</source>
        <translation>Save the plot</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1125"/>
        <source>Projet Pymecavideo (*.mecavideo)</source>
        <translation>Pymecavideo project file (*.mecavideo)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1441"/>
        <source>Enregistrer le projet pymecavideo</source>
        <translation>Save the project in pymecavideo format</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1441"/>
        <source>Projet pymecavideo (*.mecavideo)</source>
        <translation>Pymecavideo project file (*.mecavideo)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2168"/>
        <source>fichiers images(*.png)</source>
        <translation>Image files (*.png)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="395"/>
        <source>indéf</source>
        <translation>undef</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1420"/>
        <source>temps en seconde, positions en mètre</source>
        <translation>time in second, positions in meter</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2254"/>
        <source>Vous avez atteint la fin de la vidéo</source>
        <translation>You reached the end of the video</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2587"/>
        <source>Quelle est la longueur en mètre de votre étalon sur l'image ?</source>
        <translation>Which the length (in meter) of your gauge in the image?</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2610"/>
        <source> Merci d'indiquer une échelle valable</source>
        <translation>Please give a valid scale</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2670"/>
        <source>Les données seront perdues</source>
        <translation>Data will be lost</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2670"/>
        <source>Votre travail n'a pas été sauvegardé
Voulez-vous les sauvegarder ?</source>
        <translation>Your work has not been saved.
Do you want to save it?</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2747"/>
        <source>fichiers vidéos ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</source>
        <translation>video files (*.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2762"/>
        <source>Le nom de votre fichier contient des caractères accentués ou des espaces.
Merci de bien vouloir le renommer avant de continuer</source>
        <translation>The name of the file contains accented characters or spaces.
PLease rename it before going on</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="254"/>
        <source>Points à 
				étudier :</source>
        <translation type="obsolete">Points to 
study:</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="414"/>
        <source>Abscisses 
<byte value="x9"/><byte value="x9"/><byte value="x9"/>    vers la gauche</source>
        <translation type="obsolete">Abscissa to the left
</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="434"/>
        <source>Ordonnées 
			    vers le bas</source>
        <translation type="obsolete">Ordinates
     to the bottom</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="528"/>
        <source>Incrémenter le compteur d'image à chaque pointage</source>
        <translation>Increment the image count at each capture</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="518"/>
        <source>Incr :</source>
        <translation>Incr :</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="892"/>
        <source>Montrer les
<byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/>  vecteurs vitesses</source>
        <translation>Show 
velocity 
vectors</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="254"/>
        <source>Points à 
étudier :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="434"/>
        <source>Ordonnées 
 vers le bas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2233"/>
        <source>Pointage des positions : cliquer sur le point N° {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="473"/>
        <source>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p&amp;gt;Le nombre d'images par seconde est détecté automatiquement. Enter la valeur manuellement si la détection échoue.&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1649"/>
        <source>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p&amp;gt;Enregistre les données du projet pour pouvoir être réouvert dans PyMecaVideo.&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>self.app</name>
    <message>
        <location filename="../../src/cadreur.py" line="55"/>
        <source>Presser la touche ESC pour sortir</source>
        <translation>Type ESC to escape</translation>
    </message>
    <message>
        <location filename="../../src/echelle.py" line="167"/>
        <source>Choisir le nombre de points puis Â«Â DÃ©marrer l'acquisitionÂ Â» </source>
        <translation type="obsolete">Choose the number of points to track and then &amp;quot;Start Aquisition&amp;quot; </translation>
    </message>
    <message>
        <location filename="../../src/echelle.py" line="188"/>
        <source>Vous pouvez continuer votre acquisition</source>
        <translation>You can keep on with the acquisition</translation>
    </message>
    <message>
        <location filename="preferences.py" line="48"/>
        <source>Proximite de la souris %1</source>
        <translation type="obsolete">Near the mouse %1</translation>
    </message>
    <message>
        <location filename="preferences.py" line="49"/>
        <source>; derniere video %1</source>
        <translation type="obsolete">: last video %1</translation>
    </message>
    <message>
        <location filename="preferences.py" line="50"/>
        <source>; videoDir %1</source>
        <translation type="obsolete">: videoDir %1</translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="47"/>
        <source>Proximite de la souris {0}</source>
        <translation>Near the mouse {0}</translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="49"/>
        <source>; derniere video {0}</source>
        <translation>; last video {0}</translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="50"/>
        <source>; videoDir {0}</source>
        <translation>; videoDir {0}</translation>
    </message>
    <message>
        <location filename="../../src/cadreur.py" line="143"/>
        <source>Choisir le ralenti</source>
        <translation type="obsolete">Choose the render rate</translation>
    </message>
    <message>
        <location filename="../../src/echelle.py" line="180"/>
        <source>Choisir le nombre de points puis « Démarrer l'acquisition » </source>
        <translation>Choose the number of points to track and then &amp;quot;Start Aquisition&amp;quot; </translation>
    </message>
</context>
</TS>
