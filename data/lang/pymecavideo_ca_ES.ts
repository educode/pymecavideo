<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>MontreFilm</name>
    <message>
        <location filename="../../src/cadreur.py" line="211"/>
        <source>Voir la vidéo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/cadreur.py" line="212"/>
        <source>Ralenti : 1/1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pymecavideo</name>
    <message>
        <location filename="../../src/pymecavideo.py" line="480"/>
        <source>indéf.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2013"/>
        <source>Choisir ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1125"/>
        <source>Ouvrir un projet Pymecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2762"/>
        <source>Nom de fichier non conforme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="688"/>
        <source>fichiers images(*.png *.jpg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1495"/>
        <source>point N° {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2833"/>
        <source>Désolé pas de fichier d'aide pour le langage {0}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2731"/>
        <source>fichiers vidéos (*.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2798"/>
        <source>Veuillez choisir une image (et définir l'échelle)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2913"/>
        <source>Pymecavideo n'arrive pas à lire l'image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1519"/>
        <source>Pointage Automatique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1114"/>
        <source>le format de cette vidéo n'est pas pris en charge par pymecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1293"/>
        <source>Le fichier choisi n&apos;est pas compatible avec pymecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2385"/>
        <source>Masse de l&apos;objet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2385"/>
        <source>Quelle est la masse de l&apos;objet ? (en kg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2403"/>
        <source> Merci d&apos;indiquer une masse valable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2709"/>
        <source>MAUVAISE VALEUR !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2709"/>
        <source>La valeur rentrée n'est pas compatible avec le calcul</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="37"/>
        <source>PyMecaVideo, analyse mécanique des vidéos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="123"/>
        <source>Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="533"/>
        <source>Définir l'échelle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="236"/>
        <source>px/m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="287"/>
        <source>Démarrer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="384"/>
        <source>Tout réinitialiser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="404"/>
        <source>Changer d&apos;origine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="926"/>
        <source>Image n°</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="787"/>
        <source>Trajectoires</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="842"/>
        <source>près de la souris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="852"/>
        <source>partout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="875"/>
        <source>Échelle de vitesses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="882"/>
        <source>px pour 1 m/s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1484"/>
        <source>Enregistrer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1056"/>
        <source>Voir la vidéo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1089"/>
        <source>Coordonnées</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1095"/>
        <source>Tableau des dates et des coordonnées</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1379"/>
        <source>9.8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1326"/>
        <source>Grapheur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1423"/>
        <source>en fonction de </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1559"/>
        <source>&amp;Fichier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1670"/>
        <source>&amp;Aide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1591"/>
        <source>&amp;Edition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1620"/>
        <source>avanceimage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1625"/>
        <source>reculeimage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1661"/>
        <source>À &amp;propos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1682"/>
        <source>&amp;Exemples ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1696"/>
        <source>&amp;Préférences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1743"/>
        <source>LibreOffice &amp;Calc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1748"/>
        <source>Qti&amp;plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1753"/>
        <source>Sci&amp;davis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1758"/>
        <source>&amp;Python (source)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="451"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nombre d&apos;Images Par Seconde&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="454"/>
        <source>IPS :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="476"/>
        <source>000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="604"/>
        <source>Tourner l'image de 90° vers la gauche</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="633"/>
        <source>Tourner l'image de 90° vers la droite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="815"/>
        <source>Trajectoire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="912"/>
        <source>Chronophotographie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="825"/>
        <source>Chronogramme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1125"/>
        <source>Copier les mesures dans le presse papier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1359"/>
        <source>1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1260"/>
        <source>g (N/kg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1433"/>
        <source>avec le style </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1441"/>
        <source>Points seuls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1450"/>
        <source>Points et lignes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1459"/>
        <source>Lignes seules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1763"/>
        <source>&amp;Fichier numpy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1766"/>
        <source>Fichier Numpy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="688"/>
        <source>Enregistrer comme image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2176"/>
        <source>Erreur lors de l&apos;enregistrement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2587"/>
        <source>Définir léchelle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2858"/>
        <source>Le nombre d'images par seconde doit être un entier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2858"/>
        <source>merci de recommencer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="297"/>
        <source>STOP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="707"/>
        <source>Efface le point précédent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="745"/>
        <source>Rétablit le point suivant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1521"/>
        <source>Veuillez sélectionner un cadre autour du ou des objets que vous voulez suivre.
Vous pouvez arrêter à tout moment la capture en appuyant sur le bouton STOP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1612"/>
        <source>&amp;Ouvrir une vidéo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1634"/>
        <source>&amp;Quitter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1705"/>
        <source>&amp;Copier dans le presse-papier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1717"/>
        <source>&amp;Défaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1735"/>
        <source>Refaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1729"/>
        <source>&amp;Refaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1183"/>
        <source>Changer d'échelle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1413"/>
        <source>Tracer :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1208"/>
        <source>Ajouter les énergies :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1228"/>
        <source>Cinétique (échelle obligatoire)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1235"/>
        <source>Potentielle de pesanteur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1242"/>
        <source>Mécanique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="277"/>
        <source>Suivi automatique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1215"/>
        <source>Intensité de la pesanteur :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1338"/>
        <source>Données et grandeurs à représenter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1346"/>
        <source>Masse (kg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1366"/>
        <source>Intensité de la pesanteur g (N/kg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="102"/>
        <source>Pointage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1006"/>
        <source>Changement de référentiel : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1563"/>
        <source>E&amp;xporter vers ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1646"/>
        <source>&amp;Enregistrer le projet mecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1691"/>
        <source>Ouvrir un projet &amp;mecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2176"/>
        <source>Echec de l&apos;enregistrement du fichier:&lt;b&gt;
{0}&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2168"/>
        <source>Enregistrer le graphique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1125"/>
        <source>Projet Pymecavideo (*.mecavideo)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1441"/>
        <source>Enregistrer le projet pymecavideo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1441"/>
        <source>Projet pymecavideo (*.mecavideo)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2168"/>
        <source>fichiers images(*.png)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="395"/>
        <source>indéf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1420"/>
        <source>temps en seconde, positions en mètre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2254"/>
        <source>Vous avez atteint la fin de la vidéo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2587"/>
        <source>Quelle est la longueur en mètre de votre étalon sur l'image ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2610"/>
        <source> Merci d'indiquer une échelle valable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2670"/>
        <source>Les données seront perdues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2670"/>
        <source>Votre travail n'a pas été sauvegardé
Voulez-vous les sauvegarder ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2767"/>
        <source>Ouvrir une vidéo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2747"/>
        <source>fichiers vidéos ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2762"/>
        <source>Le nom de votre fichier contient des caractères accentués ou des espaces.
Merci de bien vouloir le renommer avant de continuer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="528"/>
        <source>Incrémenter le compteur d'image à chaque pointage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="518"/>
        <source>Incr :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="892"/>
        <source>Montrer les
<byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/>  vecteurs vitesses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="254"/>
        <source>Points à 
étudier :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="414"/>
        <source>Abscisses 
vers la gauche</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="434"/>
        <source>Ordonnées 
 vers le bas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2233"/>
        <source>Pointage des positions : cliquer sur le point N° {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="473"/>
        <source>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p&amp;gt;Le nombre d'images par seconde est détecté automatiquement. Enter la valeur manuellement si la détection échoue.&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1649"/>
        <source>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p&amp;gt;Enregistre les données du projet pour pouvoir être réouvert dans PyMecaVideo.&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>self.app</name>
    <message>
        <location filename="../../src/cadreur.py" line="55"/>
        <source>Presser la touche ESC pour sortir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/echelle.py" line="188"/>
        <source>Vous pouvez continuer votre acquisition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="47"/>
        <source>Proximite de la souris {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="49"/>
        <source>; derniere video {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="50"/>
        <source>; videoDir {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/echelle.py" line="180"/>
        <source>Choisir le nombre de points puis « Démarrer l'acquisition » </source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
